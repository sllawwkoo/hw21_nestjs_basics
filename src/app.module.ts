import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommonModule } from './common/common.module';
import { RecordModule } from './record/record.module';
import {ConfigModule} from '@nestjs/config';
import { CheckAuthKeyMiddleware } from './check-auth-key/check-auth-key.middleware';

@Module({
  imports: [ConfigModule.forRoot(), CommonModule, RecordModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
	configure(consumer: MiddlewareConsumer) {
		console.log('Configure...');

		consumer.apply(CheckAuthKeyMiddleware).forRoutes('record');
	}
}
