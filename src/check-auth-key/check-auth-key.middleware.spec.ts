import { CheckAuthKeyMiddleware } from './check-auth-key.middleware';

describe('CheckAuthKeyMiddleware', () => {
  it('should be defined', () => {
    expect(new CheckAuthKeyMiddleware()).toBeDefined();
  });
});
