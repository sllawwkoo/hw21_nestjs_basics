import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class CheckAuthKeyMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const apiKey = req.headers['api-key'];

    if (!apiKey) {
      return res.status(401).json({
        message: 'API key not found',
      });
    }

		console.log('process.env.API_KEY', process.env.API_KEY);

    if (apiKey !== process.env.API_KEY) {
      return res.status(401).json({
        message: 'Invalid API key',
      });
    }

    next();
  }
}
