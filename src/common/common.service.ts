import { Injectable } from '@nestjs/common';

@Injectable()
export class CommonService {
	encode(str: string) {
		return Buffer.from(str).toString('base64');
	}

	decode(str: string) {
		return Buffer.from(str, 'base64').toString('utf8');
	}
}
