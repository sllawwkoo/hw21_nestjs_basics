import { Module } from '@nestjs/common';
import { RecordService } from './record.service';
import { RecordController } from './record.controller';
import { CommonService } from 'src/common/common.service';

@Module({
  controllers: [RecordController],
  providers: [RecordService, CommonService],
})
export class RecordModule {}
