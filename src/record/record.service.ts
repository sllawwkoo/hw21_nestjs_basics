import { Injectable } from '@nestjs/common';
import { CreateRecordDto } from './dto/create-record.dto';
import { UpdateRecordDto } from './dto/update-record.dto';
import { CommonService } from 'src/common/common.service';

const records = [];

@Injectable()
export class RecordService {
	constructor(private readonly commonService: CommonService) {}
  create(createRecordDto: CreateRecordDto) {
    const id = Math.floor(Math.random() * 1000) + 1000;

    const record = {
      ...createRecordDto,
      content: this.commonService.encode(createRecordDto.content),
			id
    };

    records.push(record);

		console.log(records);

    return record;
  }

  findAll() {
    return records;
  }

  findOne(id: number, decode = false) {
    const record =  records.find((record) => record.id === id);

		if (!record) {
			return id
		}

		if (decode) {
			record.content = this.commonService.decode(record.content);
		}

		return record;
  }

  update(id: number, updateRecordDto: UpdateRecordDto) {
		const recordIndex = records.indexOf(id);

		if (recordIndex === -1) {
			return `Record #${id} not found`;
		}

		records[recordIndex] = this.commonService.encode(
			JSON.stringify({
				...records[recordIndex],
				...updateRecordDto,
			})
		);

    return records[recordIndex];
  }

  remove(id: number) {
		const recordIndex = records.indexOf(id);

		if (recordIndex === -1) {
			return `Record #${id} not found`;
		}

		records.splice(recordIndex, 1);

		return `Record #${id} deleted`;
  }
}
