const main = async () => {
	const addRecord = await fetch('http://localhost:3000/record', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'api-key': 'secret-key'
		},
		body: JSON.stringify({
			name: 'John Doe',
			content: 'Hello world'
		}),
	});

	const addedRecord = await addRecord.json();

	console.log('addedRecord', addedRecord);

	const allRecords = await fetch('http://localhost:3000/record', {
    // headers: {
    //   'api-key': 'secret-key',
    // },
  });

	console.log('all records', await allRecords.json());

	const record = await fetch(`http://localhost:3000/record/${addedRecord.id}`, {
    headers: {
      'api-key': 'secret-key',
    },
  });

	console.log('get record', await record.json());

	const recordDecoded = await fetch(
    `http://localhost:3000/record/${addedRecord.id}/decoded`,
    {
      headers: {
        'api-key': 'secret-key!!!',
      },
    },
  );

	console.log('decoded record', await recordDecoded.json());
}

main()